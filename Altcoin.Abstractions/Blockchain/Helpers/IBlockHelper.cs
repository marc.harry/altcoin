﻿using Altcoin.Core;

namespace Altcoin.Abstractions.Blockchain.Helpers
{
    public interface IBlockHelper
    {
        string GetBlockHeader(Block block, uint? proof = null);
        string GetMerkleRoot(Block block);
        byte[] ResolveTransactionId(Transaction transaction);
    }
}