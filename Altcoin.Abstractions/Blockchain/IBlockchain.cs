﻿using System.Collections.Generic;
using Altcoin.Core;

namespace Altcoin.Abstractions.Blockchain
{
    public interface IBlockchain
    {
        string GetHash(Core.Block block);
        Core.Block Mine();
        Core.Block CreateNewBlock(string previousHash = null);
        IList<Core.Block> GetChain();
        void AddTransaction(string from, string to, long amount);
    }
}