﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Altcoin.Core.Models;

namespace Altcoin.Abstractions.Blockchain.Services
{
    public interface IMerkleTreeService
    {
        Task<MerkleNode> BuildTree(ICollection<byte[]> nodes);
    }
}