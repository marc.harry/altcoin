﻿using System.Collections.Generic;
using Altcoin.Core;

namespace Altcoin.Abstractions.Blockchain.Services
{
    public interface ITransactionService
    {
        Transaction CreateCoinbaseTx(string to, string data);
        (long Accumulated, IDictionary<string, IList<long>> UnspentOutputs) FindSpendableOutputs(string address, long amount);
        IDictionary<string, IList<TxOutput>> FindUTXO();
        Transaction CreateTransaction(string from, string to, long amount);
        void Sign(Transaction tx, byte[] privateKey, IDictionary<string, Transaction> previousTxs);
        bool Verify(Transaction tx, IDictionary<string, Transaction> previousTxs);
        void SignTransaction(Transaction tx, byte[] privateKey);
        bool VerifyTransaction(Transaction tx);
        Transaction GetTransaction(string id);
    }
}