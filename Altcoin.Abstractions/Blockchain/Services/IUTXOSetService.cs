﻿using Altcoin.Core;

namespace Altcoin.Abstractions.Blockchain.Services
{
    public interface IUTXOSetService
    {
        void Reindex();
        void Update(Block block);
    }
}