﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Altcoin.Abstractions.Persistence
{
    public interface IBlockRepository<T>
    {
        void Save(T block);
        T Find(Expression<Func<T, bool>> query);
        void Delete(Expression<Func<T, bool>> query);
        IEnumerable<T> FindAll();
        bool DeleteAll();
    }
}