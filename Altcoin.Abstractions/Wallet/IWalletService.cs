﻿namespace Altcoin.Abstractions.Wallet
{
    public interface IWalletService
    {
        Core.Wallets.Wallet CreateNewWallet(string walletName);
        bool LoadWalletsFromFile();
        string GetAddress(Core.Wallets.Wallet wallet);
        Core.Wallets.Wallet GetWallet(string address);
        long GetBalance(string address);
    }
}