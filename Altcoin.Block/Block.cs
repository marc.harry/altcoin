﻿using System;
using System.Collections.Generic;
using Altcoin.Core.Models;

namespace Altcoin.Core
{
    public class Block : IOrderable
    {
        public int Index { get; set; }
        public uint Version { get; set; }
        public string PreviousHash { get; set; }
        public string Hash { get; set; }
        public IList<Transaction> Transactions { get; set; }
        public uint Nonce { get; set; }
        public DateTime Time { get; set; }
        public byte[] MerkleRoot { get; set; }
    }
}