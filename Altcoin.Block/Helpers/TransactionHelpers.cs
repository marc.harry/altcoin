﻿using System.Linq;
using Altcoin.Core.Security;

namespace Altcoin.Core.Helpers
{
    public static class TransactionHelpers
    {
        public static bool IsCoinbase(this Transaction transaction)
        {
            return transaction.TxInputs.First().Vout == -1;
        }

        public static void Lock(this TxOutput output, string address)
        {
            var publicKeyHash = Base58.Decode(address);
            publicKeyHash = publicKeyHash.Skip(1).Take(publicKeyHash.Length - 5).ToArray();
            output.PublicKeyHash = publicKeyHash;
        }

        public static bool IsLockedWithKey(this TxOutput output, byte[] publicKeyHash)
        {
            return publicKeyHash.SequenceEqual(output.PublicKeyHash);
        }
    }
}