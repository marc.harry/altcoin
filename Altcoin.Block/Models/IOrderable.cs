﻿namespace Altcoin.Core.Models
{
    public interface IOrderable
    {
        int Index { get; set; }
    }
}