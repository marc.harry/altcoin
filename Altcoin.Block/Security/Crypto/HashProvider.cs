﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Altcoin.Core.Security.Crypto
{
    public class HashProvider : IHashProvider
    {
        public string Sha256(string data)
        {
            var sha256 = new SHA256Managed();
            var bytes = Encoding.Unicode.GetBytes(data);
            var hash = sha256.ComputeHash(bytes);

            return string.Join("", hash.Select(h => $"{h:x2}"));
        }

        public byte[] ComputeHash(string data)
        {
            var sha256 = new SHA256Managed();
            var bytes = Encoding.Unicode.GetBytes(data);
            var hash = sha256.ComputeHash(bytes);

            return hash;
        }

        public byte[] ComputeHash(byte[] data)
        {
            var sha256 = new SHA256Managed();
            var hash = sha256.ComputeHash(data);

            return hash;
        }

        public byte[] DoubleComputeHash(string data)
        {
            var sha256 = new SHA256Managed();
            var bytes = Encoding.Unicode.GetBytes(data);
            var hash = sha256.ComputeHash(sha256.ComputeHash(bytes));

            return hash;
        }

        public byte[] ComputeRIPEMDHash(byte[] data)
        {
            return RIPEMD160Managed.Create().ComputeHash(data);
        }

        public byte[] DoubleComputeHash(byte[] data)
        {
            var sha256 = new SHA256Managed();
            var hash = sha256.ComputeHash(sha256.ComputeHash(data));
            return hash;
        }

        public byte[] HashPublicKey(byte[] publicKey)
        {
            var hash = ComputeHash(publicKey);

            var ripemdHash = ComputeRIPEMDHash(hash);

            return ripemdHash;
        }
    }
}