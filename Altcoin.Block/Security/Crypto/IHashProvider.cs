﻿namespace Altcoin.Core.Security.Crypto
{
    public interface IHashProvider
    {
        string Sha256(string data);
        byte[] ComputeHash(string data);
        byte[] ComputeHash(byte[] data);
        byte[] DoubleComputeHash(string data);
        byte[] ComputeRIPEMDHash(byte[] data);
        byte[] DoubleComputeHash(byte[] data);
        byte[] HashPublicKey(byte[] publicKey);
    }
}