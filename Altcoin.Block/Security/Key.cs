﻿using System;

namespace Altcoin.Core.Security
{
    [Serializable]
    public class Key
    {
        public byte[] PublicKey { get; set; }
        public byte[] PrivateKey { get; set; }
    }
}