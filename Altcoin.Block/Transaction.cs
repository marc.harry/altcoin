﻿using System.Collections.Generic;
using Altcoin.Core.Models;

namespace Altcoin.Core
{
    public class Transaction : ITransaction
    {
        public byte[] Id { get; set; }
        public IList<TxInput> TxInputs { get; set; }
        public IList<TxOutput> TxOutputs { get; set; }
    }
}