﻿namespace Altcoin.Core
{
    public class TxInput
    {
        public byte[] TxId { get; set; }
        public long Vout { get; set; }
        public byte[] Signature { get; set; }
        public byte[] PublicKey { get; set; }
    }
}