﻿namespace Altcoin.Core
{
    public class TxOutput
    {
        public long Value { get; set; }
        public byte[] PublicKeyHash { get; set; }
    }
}