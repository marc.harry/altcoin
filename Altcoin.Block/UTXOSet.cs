﻿using System.Collections.Generic;

namespace Altcoin.Core
{
    public class UTXOSet
    {
        public string Id { get; set; }
        public IList<TxOutput> Outputs { get; set; }
    }
}