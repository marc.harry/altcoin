﻿using System;
using Altcoin.Core.Security;

namespace Altcoin.Core.Wallets
{
    [Serializable]
    public class Wallet
    {
        public Key Key { get; set; }
    }
}