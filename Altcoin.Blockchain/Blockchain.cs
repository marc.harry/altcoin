﻿using System;
using System.Collections.Generic;
using System.Linq;
using Altcoin.Abstractions.Blockchain;
using Altcoin.Abstractions.Blockchain.Helpers;
using Altcoin.Abstractions.Blockchain.Services;
using Altcoin.Abstractions.Persistence;
using Altcoin.Abstractions.Wallet;
using Altcoin.Core;
using Altcoin.Core.Security.Crypto;

namespace Altcoin.Blockchain
{
    public class Blockchain : IBlockchain
    {
        private readonly IHashProvider _hashProvider;
        private readonly IBlockRepository<Block> _blockRepository;
        private readonly IBlockRepository<Transaction> _transRepository;
        private readonly IBlockHelper _blockHelper;
        private readonly ITransactionService _transactionService;
        private readonly BlockchainSettings _settings;
        private readonly IUTXOSetService _utxoService;
        private readonly IWalletService _walletService;
        private const int Version = 1;
        private const string Difficulty = "0000";
        private ISet<Block> Chain { get; }
        private ISet<Transaction> CurrentTransactions { get; }

        public Blockchain(
            IHashProvider hashProvider, 
            IBlockRepository<Block> blockRepository,
            IBlockRepository<Transaction> transRepository,
            IBlockHelper blockHelper,
            ITransactionService transactionService,
            BlockchainSettings settings,
            IUTXOSetService utxoService,
            IWalletService walletService
            )
        {
            _hashProvider = hashProvider;
            _blockRepository = blockRepository;
            _transRepository = transRepository;
            _blockHelper = blockHelper;
            _transactionService = transactionService;
            _settings = settings;
            _utxoService = utxoService;
            _walletService = walletService;
            Chain = new HashSet<Block>();
            CurrentTransactions = new HashSet<Transaction>();
            InitializeChain(settings.MiningAddress);
        }

        private void InitializeChain(string address)
        {
            _walletService.LoadWalletsFromFile();
            
            if (string.IsNullOrWhiteSpace(address))
            {
                address = GetNewWallet();
            }
            else
            {
                var wallet = _walletService.GetWallet(address);
                if (wallet == null)
                {
                    address = GetNewWallet();
                }
            }
            
            var blocks = _blockRepository.FindAll().ToList();
            if (blocks.Any())
            {
                foreach (var block in blocks.OrderBy(b => b.Index))
                {
                    Chain.Add(block);
                }
            }
            if (!Chain.Any())
            {
                GenerateGenesisBlock(address);
                _utxoService.Reindex();
            }
        }

        private string GetNewWallet()
        {
            var wallet = _walletService.CreateNewWallet("Miner");
            var walletAddress = _walletService.GetAddress(wallet);
            _settings.MiningAddress = walletAddress;
            return walletAddress;
        }

        public string GetHash(Block block)
        {
            // TODO: Replace this with similar method of Bitcoin (Join key values as bytes together)
            string blockText = _blockHelper.GetBlockHeader(block);
            return _hashProvider.Sha256(blockText);
        }

        public Block Mine()
        {
            // Add Transactions before this is done.

            var block = CreateNewBlock();
            ProcessProofOfWork(block);

            var rewardTx  = _transactionService.CreateCoinbaseTx(_settings.MiningAddress, "");
            block.Transactions.Add(rewardTx);
            foreach (var transaction in block.Transactions)
            {
                if (!_transactionService.VerifyTransaction(transaction))
                {
                    throw new Exception("ERROR: Invalid transaction");
                }
                _transRepository.Save(transaction);
            }

            _utxoService.Update(block);
            _blockRepository.Save(block);

            return block;
        }

        public Block CreateNewBlock(string previousHash = null)
        {
            var block = new Block
            {
                Version = Version,
                Index = Chain.Count,
                Time = DateTime.UtcNow,
                Transactions = CurrentTransactions.ToList(),
                PreviousHash = previousHash ?? GetHash(Chain.Last())
            };

            CurrentTransactions.Clear();
            Chain.Add(block);
            return block;
        }

        private uint ProcessProofOfWork(Block block)
        {
            uint proof = 0;
            while (!IsValidProof(block, proof))
                proof++;

            block.Nonce = proof;
            block.Hash = GetHash(block);
            return proof;
        }

        private bool IsValidProof(Block block, uint proof)
        {
            string guess = _blockHelper.GetBlockHeader(block, proof);
            string result = _hashProvider.Sha256(guess);
            return result.StartsWith(Difficulty);
        }

        public IList<Block> GetChain()
        {
            return Chain.ToList();
        }

        private void GenerateGenesisBlock(string address)
        {
            var block = new Block
            {
                Index = 0,
                PreviousHash = "0",
                Time = DateTime.UtcNow,
                Transactions = new List<Transaction>(),
                Version = Version
            };

            var transaction = _transactionService.CreateCoinbaseTx(address, $"First reward to test {address}: 50 CCN");
            block.Transactions.Add(transaction);
            ProcessProofOfWork(block);
            Chain.Add(block);
            _blockRepository.Save(block);
            _transRepository.Save(transaction);
        }

        public void AddTransaction(string from, string to, long amount)
        {
            var transaction = _transactionService.CreateTransaction(from, to, amount);
            CurrentTransactions.Add(transaction);
            _transRepository.Save(transaction);
        }
    }
}