﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Altcoin.Abstractions.Blockchain;
using Altcoin.Abstractions.Blockchain.Helpers;
using Altcoin.Abstractions.Blockchain.Services;
using Altcoin.Blockchain.Helpers;
using Altcoin.Blockchain.Services;
using Altcoin.Core.Security.Crypto;
using Altcoin.Persistence;
using Altcoin.Wallet;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.DependencyInjection;

namespace Altcoin.Blockchain
{
    public static class BlockchainFeature
    {
        public static IServiceCollection AddBlockchain(this IServiceCollection services, BlockchainSettings settings)
        {
            services.AddSingleton<IHashProvider, HashProvider>();
            services.AddSingleton<IBlockchain, Blockchain>();

            services.AddSingleton<IBlockHelper, BlockHelper>();
            services.AddSingleton<IMerkleTreeService, MerkleTreeService>();
            services.AddSingleton<ITransactionService, TransactionService>();
            services.AddSingleton<IUTXOSetService, UTXOSetService>();

            services.AddSingleton<BlockchainSettings>(settings);

            var assembly = typeof(BlockchainFeature).GetTypeInfo().Assembly;
            services.Configure<ApplicationPartManager>(appPartManager =>
            {
                appPartManager.ApplicationParts.Add(new AssemblyPart(assembly));
            });

            services.AddWallet();
            services.AddPersistence();

            return services;
        }

        public static IApplicationBuilder UseBlockchain(this IApplicationBuilder app)
        {
            var blockchain = (IBlockchain) app.ApplicationServices.GetService(typeof(IBlockchain));
            
            return app;
        }
    }

}