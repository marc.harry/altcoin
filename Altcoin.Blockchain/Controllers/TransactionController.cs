﻿using Altcoin.Abstractions.Blockchain.Services;
using Microsoft.AspNetCore.Mvc;

namespace Altcoin.Blockchain.Controllers
{
    [Route("api/[controller]")]
    public class TransactionController : Controller
    {
        private readonly ITransactionService _transactionService;

        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            var transaction = _transactionService.GetTransaction(id);
            return Json(transaction);
        }
    }
}