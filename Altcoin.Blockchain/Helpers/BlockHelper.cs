﻿using System.Collections.Generic;
using System.Linq;
using Altcoin.Abstractions.Blockchain.Helpers;
using Altcoin.Abstractions.Blockchain.Services;
using Altcoin.Core;
using Altcoin.Core.Security.Crypto;
using Newtonsoft.Json;

namespace Altcoin.Blockchain.Helpers
{
    public class BlockHelper : IBlockHelper
    {
        private readonly IMerkleTreeService _merkleTreeService;
        private readonly IHashProvider _hashProvider;

        public BlockHelper(IMerkleTreeService merkleTreeService, IHashProvider hashProvider)
        {
            _merkleTreeService = merkleTreeService;
            _hashProvider = hashProvider;
        }

        /// <summary>
        /// Converts all values to create the block header into byte values and combines them for hashing process
        /// </summary>
        /// <param name="block"></param>
        /// <param name="proof">Passed in when trying to find valid hash. Uses blocks Nonce value when validating block</param>
        /// <returns></returns>
        public string GetBlockHeader(Block block, uint? proof = null)
        {
            if (block.MerkleRoot == null)
            {
                block.MerkleRoot = ResolveMerkleKey(block);
            }
            // Merkle Root missing
            return $"{ByteHelper.GetInBytes(block.Version)}{block.PreviousHash}{ByteHelper.GetBytesString(block.MerkleRoot)}{ByteHelper.GetInBytes(block.Time)}{ByteHelper.GetInBytes(proof ?? block.Nonce)}";
        }

        public string GetMerkleRoot(Block block)
        {
            var bytes = ResolveMerkleKey(block);
            return ByteHelper.GetBytesString(bytes);
        }

        public byte[] ResolveMerkleKey(Block block)
        {
            var tree = _merkleTreeService.BuildTree(block.Transactions.Select(ResolveTransactionId).ToList()).Result;
            return tree.Value;
        }

        public byte[] ResolveTransactionId(Transaction transaction)
        {
            var data = JsonConvert.SerializeObject(transaction);
            return _hashProvider.ComputeHash(data);
        }
    }
}