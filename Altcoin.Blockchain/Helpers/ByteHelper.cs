﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Altcoin.Core;
using Altcoin.Core.Security.Crypto;
using Newtonsoft.Json;

namespace Altcoin.Blockchain.Helpers
{
    public class ByteHelper
    {
        public static string GetInBytes(DateTime dateTime)
        {
            var unixTime = DateHelper.GetUnixTime(dateTime);
            var unixTimeBytes = BitConverter.GetBytes(unixTime);
            var fourBytes = unixTimeBytes.Take(4).Skip(0);
            var bytes = GetBytesString(fourBytes.ToArray());

            return bytes;
        }

        public static string GetInBytes(uint number)
        {
            var bytes = BitConverter.GetBytes(number);
            return GetBytesString(bytes);
        }

        public static string GetInBytes(IList<Transaction> transactions)
        {
            var transactionText = JsonConvert.SerializeObject(transactions);
            var hash = new HashProvider().Sha256(transactionText);
            return hash;
        }

        public static byte[] GetBytes(decimal value)
        {
            var bits = decimal.GetBits(value);
            var byteArray = new List<byte>();
            foreach (var bit in bits)
            {
                var eachByte = BitConverter.GetBytes(bit);
                byteArray.AddRange(eachByte);
            }
            return byteArray.ToArray();
        }

        public static byte[] ObjectToByteArray(object obj)
        {
            var data = JsonConvert.SerializeObject(obj);
            return Encoding.UTF8.GetBytes(data);
        }

        public static string GetBytesString(byte[] bytes) => string.Join("", bytes.Select(t => $"{t:x2}"));
    }
}