﻿using System;

namespace Altcoin.Blockchain.Helpers
{
    public class DateHelper
    {
        public static long GetUnixTime(DateTime dateTime)
        {
            return ((DateTimeOffset)dateTime.ToUniversalTime()).ToUnixTimeSeconds();
        }
    }
}