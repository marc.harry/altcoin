﻿using System.Collections.Generic;
using Altcoin.Core.Models;

namespace Altcoin.Blockchain.Services.Comparer
{
    public class MerkleNodeComparer : IComparer<MerkleNode>
    {
        private readonly IComparer<byte[]> _byteArrayComparer;

        public MerkleNodeComparer()
        {
            _byteArrayComparer = new ByteArrayComparer();
        }

        public int Compare(MerkleNode x, MerkleNode y)
        {
            return _byteArrayComparer.Compare(x.Value, y.Value);
        }
    }
}