﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Altcoin.Abstractions.Blockchain.Services;
using Altcoin.Abstractions.Persistence;
using Altcoin.Abstractions.Wallet;
using Altcoin.Blockchain.Helpers;
using Altcoin.Core;
using Altcoin.Core.Security;
using Altcoin.Core.Security.Crypto;

namespace Altcoin.Blockchain.Services
{
    public class TransactionService : ITransactionService
    {
        private const long RewardSubsidy = 50;
        private readonly IBlockRepository<Block> _blockRepository;
        private readonly IHashProvider _hashProvider;
        private readonly IWalletService _walletService;
        private readonly IBlockRepository<UTXOSet> _utxoRepository;
        private readonly IBlockRepository<Transaction> _transRepository;

        public TransactionService(
            IBlockRepository<Block> blockRepository, 
            IHashProvider hashProvider,
            IWalletService walletService,
            IBlockRepository<UTXOSet> utxoRepository,
            IBlockRepository<Transaction> transRepository)
        {
            _blockRepository = blockRepository;
            _hashProvider = hashProvider;
            _walletService = walletService;
            _utxoRepository = utxoRepository;
            _transRepository = transRepository;
        }

        public Transaction CreateCoinbaseTx(string to, string data)
        {
            var txOutput = new TxOutput {Value = RewardSubsidy};
            txOutput.Lock(to);
            var transaction = new Transaction
            {
                TxInputs = new List<TxInput>
                {
                    new TxInput { TxId = new byte[] {}, Vout = -1, Signature = Encoding.UTF8.GetBytes(data) }
                },
                TxOutputs = new List<TxOutput>
                {
                    txOutput
                }
            };
            transaction.Id = Hash(transaction);

            return transaction;
        }

        public Transaction CreateTransaction(string from, string to, long amount)
        {
            var inputs = new List<TxInput>();
            var outputs = new List<TxOutput>();

            var wallet = _walletService.GetWallet(from);
            if (wallet == null)
            {
                throw new Exception($"ERROR: No wallet found for address: {from}");
            }

            var unspent = FindSpendableOutputs(from, amount);
            if (unspent.Accumulated < amount)
            {
                throw new Exception("ERROR: Not enough funds");
            }

            foreach (var outs in unspent.UnspentOutputs)
            {
                var txId = Convert.FromBase64String(outs.Key);
                foreach (var @out in outs.Value)
                {
                    var input = new TxInput { TxId = txId, Vout = @out, PublicKey = wallet.Key.PublicKey };
                    inputs.Add(input);
                }
            }

            var outputTo = new TxOutput {Value = amount};
            outputTo.Lock(to);
            outputs.Add(outputTo);
            if (unspent.Accumulated > amount)
            {
                var outputFrom = new TxOutput {Value = unspent.Accumulated - amount};
                outputFrom.Lock(from);
                outputs.Add(outputFrom);
            }

            var transaction = new Transaction
            {
                TxInputs = inputs,
                TxOutputs = outputs
            };
            transaction.Id = Hash(transaction);
            SignTransaction(transaction, wallet.Key.PrivateKey);

            return transaction;
        }

        public IDictionary<string, IList<TxOutput>> FindUTXO()
        {
            var utxo = new Dictionary<string, IList<TxOutput>>();
            var spentTxs = new Dictionary<string, IList<long>>();
            var blocks = _blockRepository.FindAll().OrderByDescending(b => b.Index);

            foreach (var block in blocks)
            {
                foreach (var transaction in block.Transactions)
                {
                    var txId = Convert.ToBase64String(transaction.Id);

                    if (!transaction.IsCoinbase())
                    {
                        foreach (var input in transaction.TxInputs)
                        {
                            var inTxId = Convert.ToBase64String(input.TxId);
                            if (!spentTxs.ContainsKey(inTxId))
                            {
                                spentTxs.Add(inTxId, new List<long>());
                            }

                            spentTxs[inTxId].Add(input.Vout);
                        }
                    }

                    foreach (var output in transaction.TxOutputs)
                    {
                        if (spentTxs.ContainsKey(txId))
                        {
                            if (spentTxs[txId].Any(spendOut => spendOut == output.Value))
                            {
                                continue;
                            }
                        }

                        if (!utxo.ContainsKey(txId))
                        {
                            utxo.Add(txId, new List<TxOutput>());
                        }

                        utxo[txId].Add(output);
                    }
                }

                if (block.PreviousHash.Length == 1)
                    break;
            }

            return utxo;
        }

        public (long Accumulated, IDictionary<string, IList<long>> UnspentOutputs) FindSpendableOutputs(string address, long amount)
        {
            var unspentOutputs = new Dictionary<string, IList<long>>();
            var utxos = _utxoRepository.FindAll();
            var accumulated = 0L;
            var decodedAddress = Base58.DecodeWithoutChecksum(address);

            foreach (var utxoSet in utxos)
            {
                var txId = utxoSet.Id;
                foreach (var output in utxoSet.Outputs)
                {
                    if (output.IsLockedWithKey(decodedAddress) && accumulated < amount)
                    {
                        accumulated += output.Value;
                        if (!unspentOutputs.ContainsKey(txId))
                        {
                            unspentOutputs.Add(txId, new List<long>());
                        }
                        unspentOutputs[txId].Add(output.Value);

                        if (accumulated >= amount)
                        {
                            break;
                        }
                    }
                }

                if (accumulated >= amount)
                {
                    break;
                }
            }

            return (accumulated, unspentOutputs);
        }

        public bool UsesKey(TxInput input, byte[] publicKeyHash)
        {
            var lockingHash = _hashProvider.HashPublicKey(input.PublicKey);

            return lockingHash.SequenceEqual(publicKeyHash);
        }

        public void Sign(Transaction tx, byte[] privateKey, IDictionary<string, Transaction> previousTxs)
        {
            if (tx.IsCoinbase())
                return;

            var txCopy = TrimmedCopy(tx);

            for (int i = 0; i < txCopy.TxInputs.Count; i++)
            {
                var prevTx = previousTxs[Convert.ToBase64String(txCopy.TxInputs[i].TxId)];
                txCopy.TxInputs[i].Signature = null;
                txCopy.TxInputs[i].PublicKey = prevTx.TxOutputs.First(o => o.Value == txCopy.TxInputs[i].Vout).PublicKeyHash;

                txCopy.Id = Hash(txCopy);
                txCopy.TxInputs[i].PublicKey = null;

                var length = tx.TxInputs[i].PublicKey.Length;
                var x = tx.TxInputs[i].PublicKey.Take(length / 2).Skip(0).ToArray();
                var y = tx.TxInputs[i].PublicKey.Skip(x.Length).ToArray();

                using (var ecdsa = ECDsa.Create())
                {
                    ecdsa.ImportParameters(new ECParameters { D = privateKey, Q = new ECPoint { X = x, Y = y }, Curve = ECCurve.NamedCurves.nistP256 });
                    var signiture = ecdsa.SignData(txCopy.Id, HashAlgorithmName.SHA256);
                    tx.TxInputs[i].Signature = signiture;
                }
            }
        }

        public bool Verify(Transaction tx, IDictionary<string, Transaction> previousTxs)
        {
            if (tx.IsCoinbase())
                return true;

            var txCopy = TrimmedCopy(tx);
            for (int i = 0; i < tx.TxInputs.Count; i++)
            {
                var prevTx = previousTxs[Convert.ToBase64String(tx.TxInputs[i].TxId)];
                txCopy.TxInputs[i].Signature = null;
                txCopy.TxInputs[i].PublicKey = prevTx.TxOutputs.First(o => o.Value == tx.TxInputs[i].Vout).PublicKeyHash;
                txCopy.Id = Hash(txCopy);
                txCopy.TxInputs[i].PublicKey = null;

                var length = tx.TxInputs[i].PublicKey.Length;
                var x = tx.TxInputs[i].PublicKey.Take(length / 2).Skip(0).ToArray();
                var y = tx.TxInputs[i].PublicKey.Skip(x.Length).ToArray();

                using (var ecdsa = ECDsa.Create())
                {
                    ecdsa.ImportParameters(new ECParameters { Q = new ECPoint { X = x, Y = y }, Curve = ECCurve.NamedCurves.nistP256 });
                    if (!ecdsa.VerifyData(txCopy.Id, tx.TxInputs[i].Signature, HashAlgorithmName.SHA256))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public Transaction FindTransaction(byte[] id)
        {
            var blocks = _blockRepository.FindAll().OrderByDescending(x => x.Index);
            foreach (var block in blocks)
            {
                foreach (var transaction in block.Transactions)
                {
                    if (transaction.Id.SequenceEqual(id))
                    {
                        return transaction;
                    }
                }

                if (block.PreviousHash.Length == 1)
                    break;
            }

            return null;
        }

        public void SignTransaction(Transaction tx, byte[] privateKey)
        {
            var prevTxs = GetPreviousTransactions(tx);

            Sign(tx, privateKey, prevTxs);
        }

        public bool VerifyTransaction(Transaction tx)
        {
            var prevTxs = GetPreviousTransactions(tx);

            return Verify(tx, prevTxs);
        }

        public Transaction GetTransaction(string id)
        {
            var transaction = _transRepository.Find(t => t.Id == Convert.FromBase64String(id));
            return transaction;
        }

        private IDictionary<string, Transaction> GetPreviousTransactions(Transaction tx)
        {
            var prevTxs = new Dictionary<string, Transaction>();
            foreach (var txInput in tx.TxInputs)
            {
                var prevTx = FindTransaction(txInput.TxId);
                if (prevTx != null)
                {
                    prevTxs.Add(Convert.ToBase64String(prevTx.Id), prevTx);
                }
            }

            return prevTxs;
        }

        private Transaction TrimmedCopy(Transaction tx)
        {
            var inputs = new List<TxInput>();
            var outputs = new List<TxOutput>();

            foreach (var txInput in tx.TxInputs)
            {
                inputs.Add(new TxInput { TxId = txInput.TxId, Vout = txInput.Vout });
            }

            foreach (var txOutput in tx.TxOutputs)
            {
                outputs.Add(new TxOutput { Value = txOutput.Value, PublicKeyHash = txOutput.PublicKeyHash });
            }

            var transaction = new Transaction
            {
                Id = tx.Id,
                TxInputs = inputs,
                TxOutputs = outputs
            };
            return transaction;
        }

        private byte[] Hash(Transaction tx)
        {
            tx.Id = new byte[]{};
            var hash = _hashProvider.ComputeHash(ByteHelper.ObjectToByteArray(tx));
            return hash;
        }
    }
}