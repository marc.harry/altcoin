﻿using System;
using System.Collections.Generic;
using Altcoin.Abstractions.Blockchain.Services;
using Altcoin.Abstractions.Persistence;
using Altcoin.Blockchain.Helpers;
using Altcoin.Core;

namespace Altcoin.Blockchain.Services
{
    public class UTXOSetService : IUTXOSetService
    {
        private readonly IBlockRepository<UTXOSet> _utxoRepository;
        private readonly ITransactionService _transactionService;

        public UTXOSetService(IBlockRepository<UTXOSet> utxoRepository, ITransactionService transactionService)
        {
            _utxoRepository = utxoRepository;
            _transactionService = transactionService;
        }

        public void Reindex()
        {
            _utxoRepository.DeleteAll();

            var utxos = _transactionService.FindUTXO();

            foreach (var utxo in utxos)
            {
                _utxoRepository.Save(new UTXOSet
                {
                    Id = utxo.Key,
                    Outputs = utxo.Value
                });
            }
        }

        public void Update(Block block)
        {
            foreach (var tx in block.Transactions)
            {
                if (!tx.IsCoinbase())
                {
                    
                    foreach (var txInput in tx.TxInputs)
                    {
                        var txId = Convert.ToBase64String(txInput.TxId);
                        var utxo = _utxoRepository.Find(x => x.Id == txId) ??
                                   new UTXOSet { Id = txId, Outputs = new List<TxOutput>() };
                        var updatedOuts = new List<TxOutput>();
                        
                        foreach (var output in utxo.Outputs)
                        {
                            if (output.Value != txInput.Vout)
                            {
                                updatedOuts.Add(output);
                            }
                        }

                        if (updatedOuts.Count == 0)
                        {
                            _utxoRepository.Delete(x => x.Id == txId);
                        }
                        else
                        {
                            utxo.Outputs = updatedOuts;
                            _utxoRepository.Save(utxo);
                        }
                    }
                }

                var id = Convert.ToBase64String(tx.Id);
                var newUtxo = _utxoRepository.Find(x => x.Id == id) ??
                           new UTXOSet { Id = id, Outputs = new List<TxOutput>() };
                foreach (var txOutput in tx.TxOutputs)
                {
                    newUtxo.Outputs.Add(txOutput);
                }
                _utxoRepository.Save(newUtxo);
            }
        }
    }
}