﻿using System;

namespace Altcoin.ConsoleApp
{
    public class Message
    {
        public string Text { get; set; }
        public Guid User { get; set; }
    }
}
