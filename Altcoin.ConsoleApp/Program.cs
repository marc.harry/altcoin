﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Altcoin.Blockchain;
using Altcoin.Blockchain.Helpers;
using Altcoin.Blockchain.Services;
using Altcoin.Core;
using Altcoin.Core.Security.Crypto;
using Altcoin.Persistence;
using Altcoin.Wallet;

namespace Altcoin.ConsoleApp
{
    class Program
    {
        private static Guid _userName;
        private const int Port = 54545;
        private const string BroadcastAddress = "255.255.255.255";
        private static UdpClient receivingClient;
        private static UdpClient sendingClient;

        static void Main(string[] args)
        {
            _userName = Guid.NewGuid();
            Console.WriteLine("Hello World!");
            InitializeClients();

            var hashProvider = new HashProvider();
            var blockRepository = new BlockRepository<Block>();
            var utxoRepository = new BlockRepository<UTXOSet>();

            var transactionRepo = new BlockRepository<Transaction>();
            var transactions = transactionRepo.FindAll().ToList();
            var walletService = new WalletService(hashProvider, utxoRepository);
            var transactionService = new TransactionService(blockRepository, hashProvider, walletService, utxoRepository, transactionRepo);
            var utxoService = new UTXOSetService(utxoRepository, transactionService);

            var walletOne = walletService.CreateNewWallet("one");
            var walletTwo = walletService.CreateNewWallet("two");

            var addressOne = walletService.GetAddress(walletOne);
            var addressTwo = walletService.GetAddress(walletTwo);

            Task.Run(() => ReceiveMessage()).ConfigureAwait(false);

            

            var blockchain = new Blockchain.Blockchain(hashProvider, blockRepository, transactionRepo,
                new BlockHelper(new MerkleTreeService(hashProvider), hashProvider), transactionService,
                new BlockchainSettings
                {
                    MiningAddress = addressOne
                }, utxoService, walletService);

            var balanceForTestAccount = walletService.GetBalance(addressOne);

            // https://jeiwan.cc/posts/building-blockchain-in-go-part-5/

            blockchain.AddTransaction(addressOne, addressTwo, 10);
            blockchain.Mine();

            var chain = blockchain.GetChain();

            balanceForTestAccount = walletService.GetBalance(addressOne);
            var balanceForRandomUser1 = walletService.GetBalance(addressTwo);

            // Clean down
//            all = blockRepository.FindAll().ToList();
//            foreach (var block1 in all)
//            {
//                blockRepository.Delete(b => b.Hash == block1.Hash);
//            }

            Console.WriteLine("Enter messages followed by enter:");
            Console.WriteLine("Type exit to close");
            while (true)
            {
                var message = Console.ReadLine();
                if (message == "exit")
                {
                    break;
                }
                SendMessage(message).Wait();
            }
        }

        public static void InitializeClients()
        {
            sendingClient = new UdpClient(BroadcastAddress, Port);
            sendingClient.EnableBroadcast = true;

            receivingClient = new UdpClient(Port);

        }

        public static async Task SendMessage(string text)
        {
            var message = new Message
            {
                Text = text,
                User = _userName
            };

            var json = JsonConvert.SerializeObject(message);
            var data = Encoding.UTF8.GetBytes(json);
            await sendingClient.SendAsync(data, data.Length);
        }

        public static void ReceiveMessage()
        {
            var ipEndpoint = new IPEndPoint(IPAddress.Parse(BroadcastAddress), Port);

            while (true)
            {
                var data = receivingClient.Receive(ref ipEndpoint);
                string json = Encoding.UTF8.GetString(data);
                var message = JsonConvert.DeserializeObject<Message>(json);
                Console.WriteLine($"{message.User}: {message.Text}");
            }
        }
    }
}
