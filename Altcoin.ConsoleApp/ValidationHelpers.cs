﻿using System;
using System.Collections.Generic;
using Altcoin.Core;
using Altcoin.Core.Security.Crypto;
using Newtonsoft.Json;

namespace Altcoin.ConsoleApp
{
    public class ValidationHelpers
    {
        public static bool ValidateDateBytes(string dateInBytes, DateTime originalDate)
        {
            byte[] bytes = new byte[dateInBytes.Length / 2];
            for (var i = 0; i < dateInBytes.Length / 2; i++)
            {
                bytes[i] = Convert.ToByte(dateInBytes.Substring(2 * i, 2), 16);
            }
            var dateTicks = BitConverter.ToInt32(bytes, 0);
            var dateTime = DateTimeOffset.FromUnixTimeSeconds(dateTicks).UtcDateTime;

            // Remove milliseconds from original date
            originalDate = originalDate.AddTicks(-originalDate.Ticks % TimeSpan.TicksPerSecond);

            return dateTime == originalDate;
        }

        public static bool ValidateMerkleRoot(string byteText, IList<Transaction> transactions)
        {
            var hash = new HashProvider().Sha256(JsonConvert.SerializeObject(transactions));

            return hash == byteText;
        }
    }
}