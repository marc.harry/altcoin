﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Altcoin.Abstractions.Persistence;
using Altcoin.Core.Models;
using LiteDB;

namespace Altcoin.Persistence
{
    public class BlockRepository<T> : IBlockRepository<T>
    {
        private readonly string _dbPath;
        private readonly BsonMapper _mapper;

        public BlockRepository()
        {
            _dbPath = $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\blockstore.db";
            _mapper = DefaultMapper.CreateMapper();
        }

        public void Save(T block)
        {
            using (var db = new LiteDatabase(_dbPath, _mapper))
            {
                var blocks = db.GetCollection<T>();


                blocks.Upsert(block);
            }
        }

        public T Find(Expression<Func<T, bool>> query)
        {
            using (var db = new LiteDatabase(_dbPath, _mapper))
            {
                var blocks = db.GetCollection<T>();

                return blocks.FindOne(query);
            }
        }

        public void Delete(Expression<Func<T, bool>> query)
        {
            using (var db = new LiteDatabase(_dbPath, _mapper))
            {
                var blocks = db.GetCollection<T>();

                blocks.Delete(query);
            }
        }

        public IEnumerable<T> FindAll()
        {
            using (var db = new LiteDatabase(_dbPath, _mapper))
            {
                var blocks = db.GetCollection<T>();

                var items = blocks.FindAll();

                if (typeof(IOrderable).IsAssignableFrom(typeof(T)))
                {
                    items = items.OrderBy(t => ((IOrderable)t).Index);
                }

                return items;
            }
        }

        public bool DeleteAll()
        {
            using (var db = new LiteDatabase(_dbPath, _mapper))
            {
                return db.DropCollection(typeof(T).Name);
            }
        }
    }
}