﻿using Altcoin.Core;
using LiteDB;

namespace Altcoin.Persistence
{
    public class DefaultMapper
    {
        public static BsonMapper CreateMapper()
        {
            var mapper = BsonMapper.Global;

            mapper.Entity<Block>()
                .Id(b => b.Hash);

            mapper.Entity<Transaction>()
                .Id(b => b.Id);

            mapper.Entity<UTXOSet>()
                .Id(b => b.Id);

            return mapper;
        }
    }
}