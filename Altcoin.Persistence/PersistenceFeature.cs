﻿using Altcoin.Abstractions.Persistence;
using Microsoft.Extensions.DependencyInjection;

namespace Altcoin.Persistence
{
    public static class PersistenceFeature
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services)
        {
            services.AddSingleton(typeof(IBlockRepository<>), typeof(BlockRepository<>));

            return services;
        }
    }
}