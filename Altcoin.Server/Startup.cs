﻿using System.Reflection;
using Altcoin.Abstractions.Blockchain;
using Altcoin.Blockchain;
using Altcoin.Persistence;
using Altcoin.Wallet;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Altcoin.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddBlockchain(new BlockchainSettings
            {
                MiningAddress = "1Nh3hrobGhpQ63fe8whihr8kDXdT28SmMN" // If empty it will create new wallet
            });

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseBlockchain();

            app.UseMvc();
        }
    }
}
