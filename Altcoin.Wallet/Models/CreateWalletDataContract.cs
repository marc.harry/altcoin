﻿using System.ComponentModel.DataAnnotations;

namespace Altcoin.Wallet.Models
{
    public class CreateWalletDataContract
    {
        [Required]
        public string Name { get; set; }
    }
}