﻿namespace Altcoin.Wallet.Models
{
    public class WalletBalanceDataContract
    {
        public string Address { get; set; }
        public long Balance { get; set; }
    }
}