﻿using Altcoin.Abstractions.Wallet;
using Altcoin.Wallet.Models;
using Microsoft.AspNetCore.Mvc;

namespace Altcoin.Wallet
{
    [Route("api/[controller]")]
    public class WalletController : Controller
    {
        private readonly IWalletService _walletService;

        public WalletController(IWalletService walletService)
        {
            _walletService = walletService;
        }

        [HttpGet("{address}/balance")]
        public IActionResult Balance(string address)
        {
            return Json(new WalletBalanceDataContract
            {
                Address = address,
                Balance = _walletService.GetBalance(address) 
            });
        }

        [HttpPost("create")]
        public IActionResult CreateWallet([FromBody]CreateWalletDataContract wallet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Json(_walletService.CreateNewWallet(wallet.Name));
        }
    }
}