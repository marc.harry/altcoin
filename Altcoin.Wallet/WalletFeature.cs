﻿using System.Reflection;
using Altcoin.Abstractions.Wallet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.DependencyInjection;

namespace Altcoin.Wallet
{
    public static class WalletFeature
    {
        public static IServiceCollection AddWallet(this IServiceCollection services)
        {
            services.AddSingleton<IWalletService, WalletService>();

            var assembly = typeof(WalletFeature).GetTypeInfo().Assembly;
            services.Configure<ApplicationPartManager>(applicationPartManager =>
            {
                applicationPartManager.ApplicationParts.Add(new AssemblyPart(assembly));
            });
            
            return services;
        }
    }
}