﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using Altcoin.Abstractions.Persistence;
using Altcoin.Abstractions.Wallet;
using Altcoin.Core;
using Altcoin.Core.Helpers;
using Altcoin.Core.Security;
using Altcoin.Core.Security.Crypto;

namespace Altcoin.Wallet
{
    public class WalletService : IWalletService
    {
        private const int CheckSumLength = 4;
        private readonly IHashProvider _hashProvider;
        private readonly IBlockRepository<UTXOSet> _utxoRepository;
        private readonly IDictionary<string, Core.Wallets.Wallet> _wallets = new Dictionary<string, Core.Wallets.Wallet>();
        private readonly string _walletFilePath;

        // TODO: Create some kind of wallet persistence separate to blockchain persistense
        public WalletService(IHashProvider hashProvider, IBlockRepository<UTXOSet> utxoRepository)
        {
            _hashProvider = hashProvider;
            _utxoRepository = utxoRepository;
            _walletFilePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        }
        
        public Core.Wallets.Wallet CreateNewWallet(string walletName)
        {
            var keyPair = NewKeyPair();
            var wallet = new Core.Wallets.Wallet
            {
                Key = new Key
                {
                    PublicKey = keyPair.Public,
                    PrivateKey = keyPair.Private
                }
            };

            _wallets.Add(GetAddress(wallet), wallet);
            var formatter = new BinaryFormatter();
            using (var stream = new FileStream($"{_walletFilePath}\\wallet-{walletName}.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                formatter.Serialize(stream, wallet);
            }

            return wallet;
        }

        private static (byte[] Public, byte[] Private) NewKeyPair()
        {
            var publicKey = new List<byte>();
            byte[] privateKey;
            using (var ecdsa = ECDsa.Create(ECCurve.NamedCurves.nistP256))
            {
                ecdsa.GenerateKey(ECCurve.NamedCurves.nistP256);
                var parameters = ecdsa.ExportParameters(true);
                privateKey = parameters.D;
                publicKey.AddRange(parameters.Q.X);
                publicKey.AddRange(parameters.Q.Y);
            }
            
            return (publicKey.ToArray(), privateKey);
        }

        private byte[] Checksum(byte[] payload)
        {
            var hash = _hashProvider.DoubleComputeHash(payload);
            return hash.Take(CheckSumLength).Skip(0).ToArray();
        }

        public string GetAddress(Core.Wallets.Wallet wallet)
        {
            var address = new List<byte>();
            var pubKeyHash = _hashProvider.HashPublicKey(wallet.Key.PublicKey);
            // Version 1
            address.AddRange(new byte[] { 0x00 });
            address.AddRange(pubKeyHash);

            var checksum = Checksum(address.ToArray());
            address.AddRange(checksum);

            // Encode Base58
            var addressValue = Base58.Encode(address.ToArray());

            var publicKeyHash = Base58.Decode(addressValue);

            if (!publicKeyHash.SequenceEqual(address))
            {
                throw new Exception("Address not generated correctly!");
            }

            return addressValue;
        }

        public Core.Wallets.Wallet GetWallet(string address)
        {
            return _wallets.TryGetValue(address, out Core.Wallets.Wallet wallet) ? wallet : null;
        }

        public IList<TxOutput> FindUTXO(string address)
        {
            var utxos = _utxoRepository.FindAll();
            var decodedAddress = Base58.DecodeWithoutChecksum(address);
            var utxo = new List<TxOutput>();

            foreach (var utxoSet in utxos)
            {
                foreach (var txOutput in utxoSet.Outputs)
                {
                    if (txOutput.IsLockedWithKey(decodedAddress))
                    {
                        utxo.Add(txOutput);
                    }
                }
            }

            return utxo;
        }

        public long GetBalance(string address)
        {
            var unspent = FindUTXO(address);
            var balance = 0L;
            foreach (var output in unspent)
            {
                balance += output.Value;
            }

            return balance;
        }

        public bool LoadWalletsFromFile()
        {
            var formatter = new BinaryFormatter();
            foreach (var file in Directory.GetFiles(_walletFilePath, "wallet*"))
            {
                try
                {
                    using (var stream = new FileStream(file, FileMode.Open, FileAccess.Read))
                    {
                        var wallet = (Core.Wallets.Wallet)formatter.Deserialize(stream);
                        _wallets.Add(GetAddress(wallet), wallet);
                    }
                }
                catch
                {
                    return false;
                }
            }

            return true;
        }
    }
}